# The purpose of this script is to check if the visible satelite is
# approaching a given city location.

import os
import math
from dotenv import load_dotenv

load_dotenv()
from skyfield.api import Topos, load
from datetime import timedelta
from pytz import timezone
from twilio.rest import Client

# Load the satellite dataset from Celestrack
starlink_url = 'https://celestrak.com/NORAD/elements/starlink.txt'
starlinks = load.tle_file(starlink_url)
print('Loaded', len(starlinks), 'satellites')

# Updating your city location and timezones
location = Topos('0 N', '37 E')
tz = timezone('Africa/Nairobi')

# Establishing time window of opportunity
ts = load.timescale()
t0 = ts.now()
t1 = ts.from_datetime(t0.utc_datetime() + timedelta(hours=2))

# Looping through satellite to find the next sighting
first_sighting = {}
for satellite in starlinks:
    # Filter out farthest satellites and NaN elevation
    elevation = satellite.at(t0).subpoint().elevation.km
    isNan = math.isnan(elevation)
    if elevation > 400 or isNan: continue
    print('Considering: {} at {}km'.format(satellite.name, round(elevation)))

    # Finding the loop through rise / set events
    t, events = satellite.find_events(location, t0, t1, altitude_degrees=30.0)
    for ti, event in zip(t, events):
        # Check if the satellite is visible to a ground observer
        eph = load('de421.bsp')
        sunlit = satellite.at(t1).is_sunlit(eph)
        if not sunlit: continue

        # Filter by moment of greatest altitude - culminate
        name = ('rise above 30°', 'culminate', 'set below 30°')[event]
        if name != 'culminate': continue

        # Find the earliest time for the next sighting
        if not first_sighting or (ti.utc < first_sighting['time']):
            first_sighting['time_object'] = ti
            first_sighting['time'] = ti.utc
            first_sighting['satellite'] = satellite

if first_sighting:
    # Create body for SMS
    next_sighting = ('Next starlink satellite sighting: {} {}'.format(
        first_sighting['satellite'].name,
        first_sighting['time_object'].astimezone(tz).strftime('%Y-%m-%d %H:%M')
    ))

    # send SMS via Twilio if there is an upcoming sighting
    account_sid = os.environ.get('TWILIO_ACCOUNT_SID')
    auth_token = os.environ.get('TWILIO_AUTH_TOKEN')
    client = Client(account_sid, auth_token)

    message = client.messages.create(
        body=next_sighting,
        from_=os.environ.get('TWILIO_PHONE_NUMBER'),
        to=os.environ.get('MY_PHONE_NUMBER')
    )

    print('Message sent:', message.sid, next_sighting)

else:
    print('No upcoming sightings')
