FROM python:3

COPY requirements.txt /
RUN pip3 install -r requirements.txt

# Adding python script
COPY tracker.py /

# Defining commands to run the script
CMD ["python3", "./tracker.py"]
